USE DWDataMart
GO

/*
SCSM Incidents

[Status_ChangeStatusId] -> 2 = New, 3 = Failed, 4 = Closed, 5 = In Progress, 6 = On Hold, 7 = Cancelled, 8 = Submitted, 9 = Completed

*/



-- Open Change Requests
SELECT COUNT(*)
FROM [DWDataMart].[dbo].[ChangeRequestDimvw]
WHERE [IsDeleted] = 0
    AND [Status_ChangeStatusId] IN (2,5,8)



-- 10 Newest Change Requests
SELECT TOP 10 [ID]
	,[Priority]
	,[Title]
	,CONVERT(VARCHAR(10),
		CONVERT(datetime, 
					SWITCHOFFSET(CONVERT(datetimeoffset, 
										[CreatedDate]),
								DATENAME(TzOffset, SYSDATETIMEOFFSET()))
				),
				101) AS Date
	,CONVERT(VARCHAR(10),
		CONVERT(datetime, 
					SWITCHOFFSET(CONVERT(datetimeoffset, 
										[CreatedDate]),
								DATENAME(TzOffset, SYSDATETIMEOFFSET()))
				),
				108) AS 'Time'

FROM [DWDataMart].[dbo].[ChangeRequestDimvw]
WHERE [IsDeleted] = 0
	AND [Status_ChangeStatusId] IN (2,5,8)
ORDER BY [CreatedDate] DESC



-- Past Service Requests
-- SET STATISTICS TIME,IO ON
SELECT [Id]
      ,REPLACE([Impact], 'ChangeImpactEnum.', '') AS [Impact]
	  ,[Title]
	  ,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME,	SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, [ScheduledStartDate])
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	  ),101) AS [Start Date]
	  ,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME, SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, [ScheduledStartDate])
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	  ),108) AS [Start Time]
      ,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME,	SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, [ScheduledEndDate])
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	  ),101) AS [End Date]
	  ,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME, SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, [ScheduledEndDate])
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	  ),108) AS [End Time]
  FROM [DWDataMart].[dbo].[ChangeRequestDimvw]
  WHERE [IsDeleted] = 0
	AND [Impact] IS NOT NULL
-- SET STATISTICS TIME,IO OFF