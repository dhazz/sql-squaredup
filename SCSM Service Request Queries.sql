USE DWDataMart
GO

/*
SCSM Service Requests
[ServiceRequestStatusId] -> 2 = New, 3 = Closed, 4 = Completed, 5 = Failed, 6 = Cancelled, 7 = On Hold,8 = In Progress, 9 = Submitted
*/

-- Open Service Requests
SELECT COUNT(*)
FROM ServiceRequestDimVw
WHERE [IsDeleted] = 0
	AND [Status_ServiceRequestStatusId] IN (2,8,9)
	AND [InternalDirective] = 0
    AND [EnhancementProject] = 0


-- 10 Newest Service Requests
-- SET STATISTICS TIME,IO ON
SELECT TOP 10 
	a.Id AS ID
    ,b.ServiceRequestPriorityValue AS [Priority]
	,a.Title AS Title
	,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME,	SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, a.CreatedDate)
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	),101) AS [Date]
	,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME, SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, a.CreatedDate)
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	),108) AS [Time]

FROM 
    dbo.[ServiceRequestDimVw] AS a
	,dbo.[ServiceRequestPriorityvw] AS b

WHERE 
    a.[Priority_ServiceRequestPriorityId] = b.[ServiceRequestPriorityId]
	AND a.[IsDeleted] = 0
	AND a.[Status_ServiceRequestStatusId] IN (2,8,9)
    AND a.[InternalDirective] = 0
    AND a.[EnhancementProject] = 0

ORDER BY a.[CreatedDate] DESC
-- SET STATISTICS TIME,IO OFF


-- 10 Oldest Service Requests
-- SET STATISTICS TIME,IO ON
SELECT TOP 10 
	a.Id AS ID
    ,b.ServiceRequestPriorityValue AS [Priority]
	,a.Title AS Title
	,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME,	SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, a.CreatedDate)
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	),101) AS [Date]
	,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME, SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, a.CreatedDate)
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	),108) AS [Time]

FROM 
    dbo.[ServiceRequestDimVw] AS a
	,dbo.[ServiceRequestPriorityvw] AS b

WHERE 
    a.[Priority_ServiceRequestPriorityId] = b.[ServiceRequestPriorityId]
	AND a.[IsDeleted] = 0
	AND a.[Status_ServiceRequestStatusId] IN (2,8,9)
    AND a.[InternalDirective] = 0
    AND a.[EnhancementProject] = 0

ORDER BY a.[CreatedDate] ASC
-- SET STATISTICS TIME,IO OFF