USE DWDataMart
GO

/*
SCSM Incidents

[Status_IncidentStatusId] -> 2 = Active, 3 = Closed, 4 = Resolved, 5 = Pending

*/

-- Open Incidents
SELECT COUNT(*)
FROM IncidentDimVw
WHERE [IsDeleted] = 0
	AND [Status_IncidentStatusId] = 2
	AND [InternalDirective] = 0


-- 10 Newest Incidents
SELECT TOP 10 [ID]
	,[Priority]
	,[Title]
	,CONVERT(VARCHAR(10),
		CONVERT(datetime, 
					SWITCHOFFSET(CONVERT(datetimeoffset, 
										[CreatedDate]),
								DATENAME(TzOffset, SYSDATETIMEOFFSET()))
				),
				101) AS Date
	,CONVERT(VARCHAR(10),
		CONVERT(datetime, 
					SWITCHOFFSET(CONVERT(datetimeoffset, 
										[CreatedDate]),
								DATENAME(TzOffset, SYSDATETIMEOFFSET()))
				),
				108) AS 'Time'

FROM [DWDataMart].[dbo].[IncidentDimvw]
WHERE [IsDeleted] = 0
	AND [Status_IncidentStatusId] IN (2,5)
	AND [InternalDirective] = 0
ORDER BY [CreatedDate] DESC


-- 10 Oldest Incidents
SELECT TOP 10 [ID]
	,[Priority]
	,[Title]
	,CONVERT(VARCHAR(10),
		CONVERT(datetime, 
					SWITCHOFFSET(CONVERT(datetimeoffset, 
										[CreatedDate]),
								DATENAME(TzOffset, SYSDATETIMEOFFSET()))
				),
				101) AS Date
	,CONVERT(VARCHAR(10),
		CONVERT(datetime, 
					SWITCHOFFSET(CONVERT(datetimeoffset, 
										[CreatedDate]),
								DATENAME(TzOffset, SYSDATETIMEOFFSET()))
				),
				108) AS 'Time'

FROM [DWDataMart].[dbo].[IncidentDimvw]
WHERE [IsDeleted] = 0 
	AND [Status_IncidentStatusId] IN (2,5)
	AND [InternalDirective] = 0
ORDER BY [CreatedDate] ASC