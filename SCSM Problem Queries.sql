USE DWDataMart
GO

/*
SCSM Problems
[Status_ProblemStatusId] -> 3 = Active, 5 = Resolved, 6 = Closed
*/

-- Open Service Requests
SELECT COUNT(*)
FROM [ProblemDimvw]
WHERE [IsDeleted] = 0
	AND [Status_ProblemStatusId] = 3


-- 10 Newest Service Requests
-- SET STATISTICS TIME,IO ON
SELECT TOP 10 
	a.Id AS ID
    ,a.Priority AS [Priority]
	,a.Title AS Title
	,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME,	SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, a.CreatedDate)
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	),101) AS [Date]
	,CONVERT(
				VARCHAR(10)
				,CONVERT(DATETIME, SWITCHOFFSET(
													CONVERT(DATETIMEOFFSET, a.CreatedDate)
													,DATENAME(TzOffset, SYSDATETIMEOFFSET())
												)
	),108) AS [Time]

FROM 
    dbo.[ProblemDimvw] AS a

WHERE a.[IsDeleted] = 0
	AND a.[Status_ProblemStatusId] = 3

ORDER BY a.[CreatedDate] DESC
-- SET STATISTICS TIME,IO OFF